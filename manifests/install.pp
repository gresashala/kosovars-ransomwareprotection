#install.pp
class ransomwareprotection::install inherits ransomwareprotection{
 case $::kernel {
  'Linux': {
    package{['clamav', 'rsync']:
      ensure => latest,
   }
  
   file{'/root/backup':
     ensure => directory,
   }

   file{ 'backup_script':
     ensure  => file,
     path    => '/root/backup/backup_script.bash',
     source  => 'puppet:///modules/ransomwareprotection/backup_script_linux',
     require => File['/root/backup'],
   }
 }# end Linux

 'Windows': {
 
   $arch              = $::architecture?{
                         'i386'  => 'x86',
                         'x86'   => 'x86',
                         'amd64' => 'x86_64',
                         'x64'   => 'x86_64',
                      }
   $cygwin_mirror_src ='http://cygwin.mirror.constant.com'
   $packages          ='openssh,wget,rsync,cygrunsrv'

  Package { provider => chocolatey,  }

  package{'ccleaner':
    ensure=>present,
  }
 
  exec {'download_cygwin':
    command  => "wget http://cygwin.com/setup-${arch}.exe -OutFile C:\Users\Administrator\setup-${arch}.exe",
    creates  => "C:\Users\Administrator\setup-${arch}.exe",
    unless   => 'Test-Path C:\Users\Administrator\setup-${arch}.exe', 
    provider => powershell,
  }
  exec {'install_cygwin':
    command  => "C:\Users\Administrator\setup-${arch}.exe -q -s ${cygwin_mirror_src} -P ${packages} -B",
    creates  => 'C:\tools\cygwin',
    unless   => 'Test-Path C:\tools\cygwin',
    provider => powershell,
    require  => Exec['download_cygwin'],
  }
  file{'C:\tools\cygwin\bin':
    ensure   => directory,
    require  => Exec['install_cygwin'],
  }
  file{'C:\Anti-Malware':
    ensure   => directory,
  } ->
 file{'Malwarebytes_download_file':
   ensure    => file,
   path      => 'C:\Anti-Malware\malwarebytes_download.ps1',
   source    => 'puppet:///modules/ransomwareprotection/malwarebytes_download',
 }
 file{'Scan_scheduler_file':
  ensure  => file,
  path    => 'C:\Anti-Malware\Scan_scheduler.ps1',
  source  => 'puppet:///modules/ransomwareprotection/scan_scheduler',
  require => File['C:\Anti-Malware'],
 } ->
 file{'Scan_Task_file':
  ensure => file,
  path   => 'C:\Anti-Malware\Scan_task.ps1',
  source => 'puppet:///modules/ransomwareprotection/scan_task',
 }
 file{'C:\backup':
  ensure => directory,
 }
 file{'backup_script':
  ensure  => file,
  path    => 'C:\backup\backup_script.ps1',
  source  => 'puppet:///modules/ransomwareprotection/backup_script',
  require => File['C:\backup'],
 }
 file{'backup_task':
  ensure  => file,
  path    => 'C:\backup\backup_task.ps1',
  source  => 'puppet:///modules/ransomwareprotection/backup_task',
  require => File['C:\backup'],
 }
 exec{'Malwarebytes_download':
  command   => '."C:\Anti-Malware\malwarebytes_download.ps1"',
  provider  => powershell,
  require   => File['Malwarebytes_download_file'],
 }
}#end Windows
}
}
