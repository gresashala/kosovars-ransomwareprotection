#
# ransomwareprotection::backup_server 
#

class ransomwareprotection::backup_server inherits ransomwareprotection{

  $manager_ssh_key = $::ransomwareprotection::manager_ssh_key
  $monitor_ssh_key = $::ransomwareprotection::monitor_ssh_key
  $login01_ssh_key = $::ransomwareprotection::login01_ssh_key
  $login02_ssh_key = $::ransomwareprotection::login02_ssh_key
  $dir01_ssh_key = $::ransomwareprotection::dir01_ssh_key
  $dir02_ssh_key= $::ransomwareprotection::dir02_ssh_key

# root@backup should be able to accept ssh req without password to all
  file{'validate_rsync':
  path => '/home/ubuntu',
  source=> 'puppet:///modules/ransomwareprotection/validate_rsync',
  mode => '+x',
  ensure => present,  
  }
  file { '/root/.ssh':
    owner    => 'root',
    group    => 'root',
    mode     => '0700',
    ensure   => 'directory',
  }
  ssh_authorized_key { 'root@manager':
    user    => 'root',
    type    => "from=${::ipaddress},command='/home/ubuntu/validate_rsync' ssh-rsa",
    key     => $manager_ssh_key,
    require => File['/root/.ssh'],
  }
  ssh_authorized_key { 'root@monitor':
    user    => 'root',
    type    => "from=${::ipaddress},command='/home/ubuntu/validate_rsync' ssh-rsa",
    key     => $monitor_ssh_key,
    require => File['/root/.ssh'],
  }
  ssh_authorized_key { 'root@login01':
    user    => 'root',
    type    => "from=${::ipaddress},command='/home/ubuntu/validate_rsync' ssh-rsa",
    key     => $login01_ssh_key,
    require => File['/root/.ssh'],
  }
  ssh_authorized_key { 'root@login02':
    user    => 'root',
    type    => "from=${::ipaddress},command='/home/ubuntu/validate_rsync' ssh-rsa",
    key     => $login02_ssh_key,
    require => File['/root/.ssh'],
  }
  ssh_authorized_key { 'Administrator@dir01':
    user    => 'root',
    type    => "from=${::ipaddress},command='/home/ubuntu/validate_rsync' ssh-rsa",
    key     => $dir01_ssh_key,
    require => File['/root/.ssh'],
  }
  ssh_authorized_key { 'Administrator@dir02':
    user    => 'root',
    type    => "from=${::ipaddress},command='/home/ubuntu/validate_rsync' ssh-rsa",
    key     => $dir02_ssh_key,
    require => File['/root/.ssh'],
}
}

