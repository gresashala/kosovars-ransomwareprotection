
class ransomwareprotection(
  $manager_ssh_key= undef,
  $monitor_ssh_key= undef,
  $dir01_ssh_key  = undef,
  $dir02_ssh_key  = undef,
  $login01_ssh_key= undef,
  $login02_ssh_key= undef,
  $logstash_server= undef,
  $scan_hour      = $ransomwareprotection::params::scan_hour,
  $scan_dir_linux = $ransomwareprotection::params::scan_dir_linux,
  $scan_minute    = $ransomwareprotection::params::scan_minute,
  $backup_hour    = $ransomwareprotection::params::backup_hour,
  $backup_minute  = $ransomwareprotection::params::backup_minute 
)inherits ransomwareprotection::params {
if(($scan_hour>=0) and ($scan_minute>=0) and ($scan_hour<=23) and ($scan_minute<=59) and ($backup_hour>=0) and ($backup_minute>=0) and ($backup_hour<=23) and ($backup_minute<=59)){

if($manager_ssh_key!= undef and $monitor_ssh_key!=undef and $dir01_ssh_key!=undef 
and $dir02_ssh_key!=undef and $login01_ssh_key!=undef and $login02_ssh_key!= undef ){

validate_string($manager_ssh_key)
validate_string($monitor_ssh_key)
validate_string($dir01_ssh_key)
validate_string($dir02_ssh_key)
validate_string($login01_ssh_key)
validate_string($login02_ssh_key)
include 'ransomwareprotection::backup_server'
}
}
else{
if( $logstash_server!=undef){
validate_string($logstash_server)
validate_string($scan_dir_linux)

include 'ransomwareprotection::monitoring::logforward'
class{'ransomwareprotection::install':}->
class{ 'ransomwareprotection::config':}
}
}
}

