class ransomwareprotection::config inherits ransomwareprotection{	
  $backup_hour =$::ransomwareprotection::backup_hour
  $backup_minute=$::ransomwareprotection::backup_minute
  $scan_hour =$::ransomwareprotection::scan_hour
  $scan_minute=$::ransomwareprotection::scan_minute
case $::kernel {
 'Windows': {
  file {'filebeat.yml':
      path    => "C:\Program Files\FIlebeat\filebeat.yml",
      content => template('ransomwareprotection/filebeat.yml'),
      ensure  => file,
      notify  => Service['filebeat'],
}
exec{'exec_backup_task':
 command     => ".'C:\backup\backup_task.ps1' -BackupHour ${backup_hour} -BackupMinute ${backup_minute}",
 require     => File['backup_task'],
 provider    => powershell,
}
exec{'Scan_task':
 command    => ".'C:\Anti-Malware\Scan_task.ps1' -ScanHour ${scan_hour} -ScanMinute ${scan_minute}",
 provider   => powershell,
 require    => File['Scan_Task_file'],
}
}
#end Windows
'Linux': {
 cron{'clamscan':
  command => "clamscan -r ${scan_dir_linux} -l /var/log/clamav/clam_scan.log; date>> /var/log/clamav/clam_scan.log; hostname>> /var/log/clamav/clam_scan.log",
  user    => 'root',
  hour    => $scan_hour,
  minute  => $scan_minute,
require   => Package['clamav'],
}
 cron{'backup_cron':
  command => '/root/backup/backup_script.bash',
  user    => 'root',
  hour    => $backup_hour,
  minute  => $backup_minute,
  require => File[backup_script],
}
}
}
}
