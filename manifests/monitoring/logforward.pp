class ransomwareprotection::monitoring::logforward inherits ransomwareprotection{
# filebeat

  $logstash_server =$::ransomwareprotection::logstash_server

# https://www.elastic.co/guide/en/beats/filebeat/current/configuring-tls-logstash.html

case $::kernel {
    'Linux'   : {
      file { [ '/etc/pki/', '/etc/pki/tls/', '/etc/pki/tls/certs/',
               '/etc/pki/tls/private/' ]:
        ensure => directory,
      } ->
      file { '/etc/pki/tls/certs/logstash.crt':
        ensure => file,
        source => 'puppet:///modules/ransomwareprotection/keys/certs/logstash.crt',
      } ->
      class { '::filebeat':
        outputs => {
          'logstash'=> {
            'hosts' => [ "${logstash_server}" ],
            'tls'   => { certificate_authorities =>
                         [ '/etc/pki/tls/certs/logstash.crt' ] }
          },
        },
        manage_repo    => true,
        package_ensure => latest,
      }
      filebeat::prospector { 'clamav':
        paths      => [
          '/var/log/clamav/clam_scan.log',
        ],
        doc_type   => 'clamscan',
        multiline  => {
              pattern => '^-+[[:space:]]SCAN[[:space:]]SUMMARY[[:space:]]-+',
              negate  => true,
              match   => after,
              },
      } # end Linux
   }
    'Windows' : {
      file { 'C:\certs':
        ensure => directory,
      } ->
      file { 'C:\certs\logstash.crt':
        ensure => file,
        source => 'puppet:///modules/ransomwareprotection/keys/certs/logstash.crt',
      } ->
      class { '::filebeat':
        outputs     => {
          'logstash'=> {
            'hosts' => [ "${logstash_server}" ],
            'tls'   => { certificate_authorities =>
                         [ 'C:\certs\logstash.crt' ] }
          },
        },
        manage_repo    => true,
        package_ensure => latest,

      }
      filebeat::prospector { 'malwarebytes':
        paths    => [
          "C:\Users\*\AppData\Roaming\Malwarebytes\Malwarebytes' Anti-Malware\Logs\mbam-log-*.txt",
        ],
        doc_type     => 'malwarebytes',
        multiline    =>{
             pattern => '^Administrator',
             negate  => true,
             match   => after,
             }
      }
    } # end Windows
  }
}
